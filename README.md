## 平台介绍

<p class="show-images"><img src="/images/undraw_winter_designer_a2m7.svg" width="40%" /></p>

### 本文档适合以下人员阅读

- 项目经理
- 设计人员
- 开发人员
- 平台管理员

### 概述

此文档用于开发人员项目指引工程，开发并不需要了解整个平台架构及内部结构，此对业务开发透明化，
开发针对文档的示例及代码生成器即可生成现成代码，进行二次开发，无需要从零配置，更专注业务实现。

企业SaaS平台
<p class="show-images"><img src="/images/10_saas_01.png" width="100%" /></p>

<!-- 构建项目 -->
<!-- <p class="show-images"><img src="/images/10_saas_02.png" width="100%" /></p> -->

### 文档说明
- 帮助开发人员快速入门，降低学习成本，提供了快速入门学习文档;
- 支持开发人员在具体项目开发中快速解决问题，提供了常用开发参考和和典型开发场景;
- 详细而全面的技术手册，供开发人员在开发过程中提供参考。

### 技术支持
- 如果无法定位问题，可以在搜索中查找关键字方式寻找帮助;
- 如果还不能解决问题，加入平台社区群【QQ群(480981318)】进行沟通交流。

### 开发技术
- [前端UI开发手册](http://gitbook.alinesno.com/document-platform-pages/)
- [开发常用工具类](https://hutool.cn/docs/#/)
- [Dubbo开发文档](http://dubbo.apache.org/zh-cn/docs/user/demos/)
- [SpringBoot启动工程](https://start.spring.io/)

<!-- - [沟通交流群](<a target="_blank" style="margin-top: 2px;position: absolute; margin-left: 10px;"  href="//shang.qq.com/wpa/qunwpa?idkey=bc44e8935e545891e1aa4ff2c2417cff0f21aef796c6ffc42bc79c462d8ee2ef"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="企业级系统架构师5群" title="企业级系统架构师5群"></a>) -->
