## 生成代码

<p class="show-images"><img src="/images/undraw_work_time_lhoj.svg" width="40%" /></p>

代码生成可生成基础的CURD和页面，自动与持续集成整合。

### 前置条件：

1. 网络连接正常；
2. 登陆账户;
3. 远程数据库存在且有效；

### 生成过程：

进入门户后台，界面如图所示，之后点击创建项目进入下一步

<p class="show-images"><img src="/images/example_01.jpg" width="80%" /></p>

##### 应用信息

样例信息填写如下
<p class="show-images"><img src="/images/example_02.png" width="80%" /></p>

##### 数据库配置

数据库配置信息填写详细如下
<p class="show-images"><img src="/images/example_03.png" width="80%" /></p>

##### 持续集成配置（git和jenkins配置)

- [必选]配置git地址
- [可选]配置jenkins

<p class="show-images"><img src="/images/example_04.png" width="80%" /></p>

##### 点击生成

点击完成。代码生成成功后，查看远程仓库，可查看到项目提交信息为“代码生成器自动生成”，至此本节完毕。

<p class="show-images"><img src="/images/example_17.png" width="80%" /></p>


###  注意事项

略
